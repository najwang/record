package main

import (
	"os"

	"github.com/01-edu/z01"
)

func main() {
	args := os.Args
	b := args[len(args)-1]
	for _, c := range b {
		z01.PrintRune(c)
	}
	z01.PrintRune('\n')
}
