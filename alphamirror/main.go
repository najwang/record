package main

import (
	"github.com/01-edu/z01"
	"os"
)

func main() {
	args := os.Args[1:]
	if len(args) != 1 {
		z01.PrintRune('\n')
		return
	}

	input := args[0]
	for _, char := range input {
		if char >= 'a' && char <= 'z' {
			z01.PrintRune('a' + 'z' - char)
		} else if char >= 'A' && char <= 'Z' {
			z01.PrintRune('A' + 'Z' - char)
		} else {
			z01.PrintRune(char)
		}
	}
	z01.PrintRune('\n')
}
