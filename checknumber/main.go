package main

import (
	"fmt"
)

func main() {
	fmt.Println(CheckNumber("Hello"))
	fmt.Println(CheckNumber("Hello1"))
}
func CheckNumber(arg string) bool {
	slice := []rune(arg)
	for _, value := range slice {
		if value >= '0' && value <= '9' {
			return true
		}
	}
	return false
}
