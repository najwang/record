package main

import "github.com/01-edu/z01"

func main() {
	str := "Hello World!"
	for _, text := range str {
		z01.PrintRune(text)
	}
	z01.PrintRune('\n')
}
