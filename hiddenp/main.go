package main

import (
	"os"

	"github.com/01-edu/z01"
)

func main() {
	args := os.Args[1:]
	if len(args) != 2 {
		return
	}
	first := args[0]
	second := []rune(args[1])
	start := 0
	b := ""
	for _, r := range first {
		for i := start; i < len(second); i++ {
			letter := second[i]
			if letter == r {
				b += string(letter)
				start = i + 1
				break
			}
		}
	}
	if b == first {
		z01.PrintRune('1')
	} else {
		z01.PrintRune('0')
	}
	z01.PrintRune('\n')
}
