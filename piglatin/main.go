package main

import (
	//"fmt"
	"os"

	"github.com/01-edu/z01"
)

func main() {
	args := os.Args[1:]
	if len(args) != 1 {
		return
	}
	word := args[0]
	if word == "" {
		return
	}
	h := []rune(word)
	str := ""
	for i, r := range h {
		if i == 0 && isVowel(r) {
			str = string(h) + "ay"
			break
		} else if isVowel(r) {
			consonantsBefore := h[0:i]
			everythingAfter := h[i:]
			str = string(everythingAfter) + string(consonantsBefore) + "ay"
			break
		} 
	}

	if str == "" {
		printString("No vowels")
	}

	printString(str)
	z01.PrintRune('\n')
}

func printString(m string) {
	for _, g := range m {
		z01.PrintRune(g)
	}
}

func isVowel(r rune) bool {
	b := []rune{'a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U'}
	for _, g := range b {
		if g == r {
			return true
		}
	}
	return false
}
