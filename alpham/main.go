package main

import (
	"github.com/01-edu/z01"
)

func main() {
	// b := "AbCdEfGhIjKlMnOpQrStUvWxYz"
	// for _, g := range b {
	// 	z01.PrintRune(g)
	// }
	for i := 'a'; i <= 'z'; i++ {
		if i%2 == 0 {
			j := i - 32
			z01.PrintRune(j)
		} else {
			z01.PrintRune(i)
		}
	}
	z01.PrintRune('\n')
}
