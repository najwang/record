package main

import (
	//"fmt"
	// "fmt"
	"os"

	"github.com/01-edu/z01"
	//"strings"
)

func main() {
	args := os.Args[1:]
	if len(args) != 1 {
		return
	}
	str := args[0]

	splits := Split(str)

	for i := len(splits) - 1; i >= 0; i-- {
		f := splits[i]
		if f == "" {
			continue
		} else {
			printString(f)
			z01.PrintRune('\n')
			break
		}
	}
}

func Split(s string) []string {
	splits := make([]string, 0)
	start := 0
	for i, g := range s {
		if g == ' ' {
			splits = append(splits, string(s[start:i]))
			start = i + 1
		}
	}

	if start != len(s) {
		splits = append(splits, string(s[start:]))
	}

	return splits
}

func printString(s string) {
	for _, k := range s {
		z01.PrintRune(k)
	}
}
