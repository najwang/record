# displayfirstparam
## Instructions

Write a program that displays its first argument, if there is one.
## Usage
```bash
$ go run . hello there
hello
$ go run . "hello there" how are you
hello there
$ go run .
$
```
