package main

import (
	"os"

	"github.com/01-edu/z01"
)

func main() {
	if len(os.Args) == 1 {
		return
	}
	args := os.Args[1:]
	arg := args[0]
	for _, b := range arg {
		z01.PrintRune(b)
	}
	z01.PrintRune('\n')
}
