package main

import (
	"os"
	"strconv"

	"github.com/01-edu/z01"
)

func main() {
	args := os.Args[1:]
	if len(args) != 1 {
		return
	}
	num, err := strconv.Atoi(args[0])
	if err != nil {
		printString("ERROR: cannot convert to roman digit")
		z01.PrintRune('\n')
		return
	}
	if num >= 4000 || num <= 0 {
		printString("ERROR: cannot convert to roman digit")
		z01.PrintRune('\n')
		return
	}
	rn, romanSums := RomanNumbers(num)
	// Print the roman sums
	for i, rs := range romanSums {
		if i != 0 {
			printString("+")
		}
		printString(rs)
	}
	z01.PrintRune('\n')
	// Print the roman number
	printString(string(rn))
	z01.PrintRune('\n')
}

func RomanNumbers(n int) ([]rune, []string) {
	var rn []rune
	var romanSums []string
	if n >= 1000 {
		for n >= 1000 {
			rn = append(rn, 'M')
			romanSums = append(romanSums, "M")
			n -= 1000
		}
	}
	if n >= 900 {
		for n >= 900 {
			rn = append(rn, 'C')
			rn = append(rn, 'M')
			romanSums = append(romanSums, "(M-C)")
			n -= 900
		}
	}
	if n >= 500 {
		for n >= 500 {
			rn = append(rn, 'D')
			romanSums = append(romanSums, "D")
			n -= 500
		}
	}
	if n >= 400 {
		for n >= 400 {
			rn = append(rn, 'C')
			rn = append(rn, 'D')
			romanSums = append(romanSums, "(D-C)")
			n -= 400
		}
	}
	if n >= 100 {
		for n >= 100 {
			rn = append(rn, 'C')
			romanSums = append(romanSums, "C")
			n -= 100
		}
	}
	if n >= 90 {
		for n >= 90 {
			rn = append(rn, 'X')
			rn = append(rn, 'C')
			romanSums = append(romanSums, "(C-X)")
			n -= 90
		}
	}
	if n >= 50 {
		for n >= 50 {
			rn = append(rn, 'L')
			romanSums = append(romanSums, "L")
			n -= 50
		}
	}
	if n >= 40 {
		for n >= 40 {
			rn = append(rn, 'X')
			rn = append(rn, 'L')
			romanSums = append(romanSums, "(L-X)")
			n -= 40
		}
	}
	if n >= 10 {
		for n >= 10 {
			rn = append(rn, 'X')
			romanSums = append(romanSums, "X")
			n -= 10
		}
	}
	if n >= 9 {
		for n >= 9 {
			rn = append(rn, 'I')
			rn = append(rn, 'X')
			romanSums = append(romanSums, "(X-I)")
			n -= 9
		}
	}
	if n >= 5 {
		for n >= 5 {
			rn = append(rn, 'V')
			romanSums = append(romanSums, "V")
			n -= 5
		}
	}
	if n >= 4 {
		for n >= 4 {
			rn = append(rn, 'I')
			rn = append(rn, 'V')
			romanSums = append(romanSums, "(V-I)")
			n -= 4
		}
	}
	if n >= 1 {
		for n >= 1 {
			rn = append(rn, 'I')
			romanSums = append(romanSums, "I")
			n -= 1
		}
	}
	return rn, romanSums
}

func printString(s string) {
	for _, b := range s {
		z01.PrintRune(b)
	}
}
