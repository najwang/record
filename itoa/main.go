package main

import (
	"fmt"
)

func main() {
	fmt.Println(Itoa(12345))
	fmt.Println(Itoa(0))
	fmt.Println(Itoa(-1234))
	fmt.Println(Itoa(987654321))
}
func Itoa(n int) string {
	if n == 0 {
		return "0"
	}
	isNegative := false
	if n < 0 {
		isNegative = true
		n = n * -1
	}
	digit := make([]int, 0)
	str := ""
	if isNegative {
		str += "-"
	}
	for n > 0 {
		digit = append(digit, n%10)
		n /= 10
	}
	for i := len(digit) - 1; i >= 0; i-- {
		str += string(rune(digit[i]) + '0')
	}
	return str
}
