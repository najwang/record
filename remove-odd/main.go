package main

import "fmt"

func main(){
    fmt.Println(RemoveOdd("Hello World"))
    fmt.Println(RemoveOdd("H"))
    fmt.Println(RemoveOdd("How are you?"))
}

func RemoveOdd(s string) string{
	r := []rune(s)
	str := ""
	for i, rr := range r {
		if i%2 == 1 || rr == ' ' {
			str += string(rr)
		}
	}
	return str
}
