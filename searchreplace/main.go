package main

import (
	"os"

	"github.com/01-edu/z01"
)

func main() {
	args := os.Args[1:]
	if len(args) != 3 {
		return
	}
	first := []rune(args[0])
	second := []rune(args[1])
	third := []rune(args[2])
	if len(second) != 1 || len(third) != 1 {
		return
	}
	for _, char := range first {
		if char == second[0] {
			char = third[0]
		}
		z01.PrintRune(char)
	}
	z01.PrintRune('\n')
}
