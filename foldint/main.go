package main

import (
	"fmt"

	"github.com/01-edu/z01"
)

func main() {
	table := []int{1, 2, 3}
	ac := 93
	FoldInt(Add, table, ac)
	FoldInt(Mul, table, ac)
	FoldInt(Sub, table, ac)
	fmt.Println()

	table = []int{0}
	FoldInt(Add, table, ac)
	FoldInt(Mul, table, ac)
	FoldInt(Sub, table, ac)
}

func FoldInt(f func(int, int) int, a []int, n int) {
	if len(a) == 0 {
		return
	}
	result := n
	for i := 0; i < len(a); i++ {
		result = f(result, a[i])
	}
	// str := strconv.Itoa(result)
	Itoa(result)
	// printString(str)
	z01.PrintRune('\n')
}

func Itoa(n int) {
	if n == 0 {
		z01.PrintRune('0')
		return
	}

	digit := make([]int, 0)
	for n > 0 {
		digit = append(digit, n%10)
		n /= 10
	}
	for i := len(digit) - 1; i >= 0; i-- {
		z01.PrintRune(rune(digit[i] + '0'))
	}
}

func Add(a, b int) int {
	return a + b
}

func Mul(a, b int) int {
	return a * b
}

func Sub(a, b int) int {
	return a - b
}

// func printString(s string) {
// 	for _, b := range s {
// 		z01.PrintRune(b)
// 	}
// }
