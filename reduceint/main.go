package main

import "github.com/01-edu/z01"

func main() {
	mul := func(acc int, cur int) int {
		return acc * cur
	}
	sum := func(acc int, cur int) int {
		return acc + cur
	}
	div := func(acc int, cur int) int {
		return acc / cur
	}
	as := []int{500, 2}
	ReduceInt(as, mul)
	ReduceInt(as, sum)
	ReduceInt(as, div)
}
func ReduceInt(a []int, f func(int, int) int) {
	if len(a) == 0 {
		return
	}
	result := a[0]
	for i := 1; i < len(a); i++ {
		result = f(result, a[i])
	}
	Num(result)
	z01.PrintRune('\n')
}
func Num(n int) {
	if n == 0 {
		return
	}
	digit := make([]int, 0)
	for n > 0 {
		digit = append(digit, n%10)
		n /= 10
	}
	for i := len(digit) - 1; i >= 0; i-- {
		z01.PrintRune(rune(digit[i]) + '0')
	}
}
