package main

import (
	"os"
	"strconv"

	"github.com/01-edu/z01"
)

func main() {
	args := os.Args[1:]
	if len(args) != 1 {
		return
	}

	num, err := strconv.Atoi(args[0])
	if err != nil {
		printBinary(0)
		z01.PrintRune('\n')
		return
	}

	printBinary(num)
	z01.PrintRune('\n')
}

func printBinary(nb int) {
	str := ""
	if nb == 0 {
		str = "00000000"
	}

	for nb > 0 {
		digit := nb % 2
		nb /= 2
		str += string(rune(digit + '0'))
	}

	for len(str) < 8 {
		str += string('0')
	}

	ss := []rune(str)

	for i := len(str) - 1; i >= 0; i-- {
		z01.PrintRune(ss[i])
	}
}
