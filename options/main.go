package main

import (
	"github.com/01-edu/z01"
	"os"
)

func main() {
	options := os.Args[1:]

	if len(options) == 0 {
		usage()
		return
	}

	m := make(map[rune]rune)
	good := false
	for _, op := range options {
		if op == "" {
			continue
		}

		for i, r := range op {
			if i == 0 && r != '-' {
				usage()
				return
			} else if i == 0 {
				continue
			}

			if i == 1 && r == 'h' {
				usage()
				return
			}

			if r >= 'a' && r <= 'z' {
				m[r] = '1'
			} else {
				printString("Invalid Option\n")
				return
			}
			good = true
		}

	}

	if !good {
		usage()
		return
	}

	output := "000000"
	for letter := 'z'; letter >= 'a'; letter-- {
		bit, ok := m[letter]
		if !ok {
			output += "0"
		} else {
			output += string(bit)
		}
	}

	for i, r := range output {
		if i != 0 && i%8 == 0 {
			z01.PrintRune(' ')
		}
		z01.PrintRune(r)

	}

	z01.PrintRune('\n')

}

func usage() {
	printString("options: abcdefghijklmnopqrstuvwxyz\n")
}

func printString(s string) {
	for _, r := range s {
		z01.PrintRune(r)
	}
}
