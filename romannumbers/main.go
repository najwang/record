package main

import (
	"os"
	"strconv"

	"github.com/01-edu/z01"
)

func main() {
	args := os.Args[1:]
	if len(args) != 1 {
		return
	}
	num, err := strconv.Atoi(args[0])
	if err != nil {
		printString("ERROR: cannot convert to roman digit")
		z01.PrintRune('\n')
		return
	}
	if num >= 4000 || num <= 0 {
		printString("ERROR: cannot convert to roman digit")
		z01.PrintRune('\n')
		return
	}
	rn := RomanNumbers(num)
	printRomanDiff(string(rn))
	z01.PrintRune('\n')
	printString(string(rn))

	z01.PrintRune('\n')
}

func RomanNumbers(n int) []rune {
	var rn []rune
	if n >= 1000 {
		for n >= 1000 {
			rn = append(rn, 'M')
			n -= 1000
		}
	}
	if n >= 900 {
		for n >= 900 {
			rn = append(rn, 'C')
			rn = append(rn, 'M')
			n -= 900
		}
	}
	if n >= 500 {
		for n >= 500 {
			rn = append(rn, 'D')
			n -= 500
		}
	}
	if n >= 400 {
		for n >= 400 {
			rn = append(rn, 'C')
			rn = append(rn, 'D')
			n -= 400
		}
	}
	if n >= 100 {
		for n >= 100 {
			rn = append(rn, 'C')
			n -= 100
		}
	}
	if n >= 90 {
		for n >= 90 {
			rn = append(rn, 'X')
			rn = append(rn, 'C')
			n -= 90
		}
	}
	if n >= 50 {
		for n >= 50 {
			rn = append(rn, 'L')
			n -= 50
		}
	}
	if n >= 40 {
		for n >= 40 {
			rn = append(rn, 'X')
			rn = append(rn, 'L')
			n -= 40
		}
	}
	if n >= 10 {
		for n >= 10 {
			rn = append(rn, 'X')
			n -= 10
		}
	}
	if n >= 9 {
		for n >= 9 {
			rn = append(rn, 'I')
			rn = append(rn, 'X')
			n -= 9
		}
	}
	if n >= 5 {
		for n >= 5 {
			rn = append(rn, 'V')
			n -= 5
		}
	}
	if n >= 4 {
		for n >= 4 {
			rn = append(rn, 'I')
			rn = append(rn, 'V')
			n -= 4
		}
	}
	if n >= 1 {
		for n >= 1 {
			rn = append(rn, 'I')
			n -= 1
		}
	}
	return rn
}

func printString(s string) {
	for _, b := range s {
		z01.PrintRune(b)
	}
}

func printRomanDiff(s string) {
	if len(s) == 1 {
		printString(s)
		return
	}

	for i := 0; i < len(s); i++ {
		r := rune(s[i])

		if i == len(s)-1 {
			z01.PrintRune('+')
			z01.PrintRune(r)
			break
		}

		a := rune(s[i])
		b := rune(s[i+1])

		if aLessThanB(a, b) {
			if i != 0 {
				z01.PrintRune('+')
			}

			z01.PrintRune('(')
			z01.PrintRune(b)
			z01.PrintRune('-')
			z01.PrintRune(a)
			z01.PrintRune(')')
			i++
		} else {
			if i != 0 {
				z01.PrintRune('+')
			}
			z01.PrintRune(r)

		}

	}
}

func aLessThanB(a, b rune) bool {
	m := map[rune]int{
		'I': 1, 'V': 5, 'X': 10,
		'L': 50, 'C': 100, 'D': 500, 'M': 1000,
	}

	return m[a] < m[b]
}
