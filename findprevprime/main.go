package main

import "fmt"

func main() {
	fmt.Println(FindPrevPrime(6))
	fmt.Println(FindPrevPrime(4))
}
func FindPrevPrime(nb int) int {
	if nb <= 2 {
		return 2
	}
	if nb%2 == 0 {
		nb--
	}
	for {
		if isPrime(nb) {
			return nb
		}
		nb -= 2
	}

}
func isPrime(n int) bool {
	if n <= 1 {
		return false
	}
	for i := 2; i <= n/2; i++ {
		if n%i == 0 {
			return false
		}
	}
	return true
}
