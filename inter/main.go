package main

import (
	"os"

	"github.com/01-edu/z01"
)

func main() {
	args := os.Args[1:]
	if len(args) != 2 {
		return
	}
	first := args[0]
	second := []rune(args[1])
	str := ""
	for _, b := range first {
		for i := 0; i < len(second); i++ {
			letter := second[i]
			if letter == b {
				if !Contains(letter, str) {
					str += string(letter)
				}
				break
			}
		}
	}

	printString(str)
	z01.PrintRune('\n')
}
func printString(s string) {
	for _, b := range s {
		z01.PrintRune(b)
	}
}
func Contains(r rune, s string) bool {
	for _, g := range s {
		if g == r {
			return true
		}
	}
	return false
}
