package main

import (
	"github.com/01-edu/z01"
	"os"
	"strconv"
)

func main() {
	args := os.Args[1:]
	if len(args) != 1 {
		return
	}
	num, err := strconv.Atoi(args[0])
	if err != nil {
		printString("ERROR: cannot convert to roman digit\n")
		return
	}
	if num >= 4000 || num <= 0 {
		printString("ERROR: cannot convert to roman digit\n")
		return
	}
	rn, rnDiff := RomanNumbers(num)
	printString(rnDiff + "\n")
	printString(rn + "\n")
}

func RomanNumbers(num int) (rn string, diff string) {
	m := map[int]string{
		1:    "I",
		4:    "IV",
		5:    "V",
		9:    "IX",
		10:   "X",
		40:   "XL",
		50:   "L",
		90:   "XC",
		100:  "C",
		400:  "CD",
		500:  "D",
		900:  "CM",
		1000: "M",
	}
	l := []int{1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1}

	rn, ok := m[num]
	if ok {
		if len(rn) == 2 {
			diff = "(" + string(rn[1]) + "-" + string(rn[0]) + ")"
		} else {
			diff = rn
		}
	} else {
		count := 0
		for _, r := range l {
			for num >= r {
				num -= r
				currentRoman := m[r]
				rn += currentRoman

				if count > 0 {
					diff += "+"
				}

				if len(currentRoman) == 2 {
					diff += "(" + string(currentRoman[1]) + "-" + string(currentRoman[0]) + ")"
				} else {
					diff += currentRoman
				}
				count++
			}
		}

	}

	return
}

func printString(s string) {
	for _, b := range s {
		z01.PrintRune(b)
	}
}
