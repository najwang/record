package main

import (
	"os"

	"github.com/01-edu/z01"
)

func main() {
	args := os.Args[1:]
	if len(args) != 1 {
		return
	}
	bee := args[0]
	printString(bee)
}

func printString(s string) {
	d := []rune(s)
	for i, r := range d {
		if r >= 'a' && r <= 'z' {
			d[i] = 'a' + (r-'a'+13)%26
		} else if r >= 'A' && r <= 'Z' {
			d[i] = 'A' + (r-'A'+13)%26
		}
		z01.PrintRune(d[i])
	}
	z01.PrintRune('\n')
}
