package main

import "os"

func main() {
	args := os.Args[1:]
	if len(args) != 1 {
		return
	}
	first := args[0]
	n := 0
	for _, r := range first {
		n = n*10 + int(r-'0')
	}
	if n <= 0 {
		return
	}

	if Power2(n) {
		print("true")
	} else {
		print("false")
	}
	print("\n")
}

func Power2(n int) bool {
	// 2^3 = 2 * 2 * 2
	// n := 16
	// 2^0 = 1
	// 2^1 = 2

	if n == 1 {
		return true
	}

	result := 2
	for result <= n {
		if result == n {
			return true
		}
		result *= 2
	}

	return false
}
