package main

import (
	"os"
)

func Itoa(num int) string {
	sign := ""
	result := ""

	if num == 0 {
		return "0"
	} else if num == -9223372036854775808 {
		// MinInt can't be negated as -MinInt, it will overflow
		return "-9223372036854775808"
	} else if num < 0 {
		sign = "-"
		num = -num
	}

	for num > 0 {
		digStr := string(rune((num % 10) + '0'))
		result = digStr + result
		num /= 10
	}

	return sign + result
}

func Atoi(str string) (int, bool) {
	var result int
	sign := 1

	for i, char := range str {
		if i == 0 && char == '-' {
			sign = -1
			continue
		} else if i == 0 && char == '+' {
			continue
		} else if char < '0' || char > '9' {
			return 0, true
		}

		dig := int(char - '0')
		if multiplicationWillOverflow(result, 10) {
			return 0, true
		}
		if addWillOverflow(result*10, dig) {
			return 0, true
		}
		result = (result * 10) + dig
	}

	result = result * sign
	return result, false
}

func main() {
	// First use the "math" package to get (math.MinInt) and (math.MaxInt)
	// MinInt: -9223372036854775808
	// MaxInt: 9223372036854775807
	//fmt.Printf("MinInt: %d\n", math.MinInt)
	//fmt.Printf("MaxInt: %d\n", math.MaxInt)

	// Ignore invalid program args
	if len(os.Args[1:]) != 3 {
		return
	}

	// go run . 1 + 2
	a, err := Atoi(os.Args[1])
	if err {
		return
	}
	ops := os.Args[2]
	b, err := Atoi(os.Args[3])
	if err {
		return
	}
	var ans int
	switch ops {
	case "-":
		if subWillOverflow(a, b) {
			return
		}
		ans = a - b
	case "+":
		if addWillOverflow(a, b) {
			return
		}
		ans = a + b
	case "*":
		if multiplicationWillOverflow(a, b) {
			return
		}
		ans = a * b
	case "/":
		if b == 0 {
			printString("No division by 0\n")
			return
		}
		ans = a / b
	case "%":
		if b == 0 {
			printString("No modulo by 0\n")
			return
		}
		ans = a % b
	default:
		return
	}

	printString(Itoa(ans))
	printString("\n")
}

// print the string s, ignoring any stdout errors
func printString(s string) {
	_, _ = os.Stdout.WriteString(s)
}

// Note:
// Operations on division will NEVER overflow; thus, it follows that, operations on modulo will NEVER overflow.
// Thus, we use division to confirm overflows on multiplication
// We know 8 * 3 = 24; but, it must also hold that 24/3 = 8. It should also be true that 24/8 = 3
// Therefore, it follows that a * b = c; but, it must also hold that c/b = a. It should alo hold that c/a = b
func multiplicationWillOverflow(a, b int) bool {
	c := a * b
	if a != 0 {
		return c/a != b
	} else if b != 0 {
		return c/b != a
	}
	return false
}

// To get the reasoning behind these addition and subtraction functions,
// consider a number-line from -10 to 9; [-10_________0________9]
// where, the maximum number possible is 9 and the minimum number possible is -10.
//
// Taking the minimum number (-10), we know (-10-1=-11), but we can't represent -11 in this number-line,
// hence, it underflows and thus, becomes that (-10-1=9), that is, it circles around.
// Now, with reference to 0; -10 < 0, -1 < 0, but 9 > 0
//
// Now, taking the maximum number (9), we know (9+1=10), but we can't represent 10 in this number-line,
// hence, it overflows and thus, becomes that (9+1=-10), that is, it circles around.
// Now, with reference to 0; 9 > 0; 1 > 0; but -10 < 0
func addWillOverflow(a, b int) bool {
	c := a + b
	return a > 0 && b > 0 && c < 0
}

func subWillOverflow(a, b int) bool {
	c := a - b
	return a < 0 && b < 0 && c > 0
}
