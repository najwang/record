#!/usr/bin/env bash

go run .
go run . 1 + 1 | cat -e
#2
#
go run . hello + 1
go run . 1 p 1
go run . 1 / 0 | cat -e
#No division by 0
#
go run . 1 % 0 | cat -e
#No modulo by 0
#
go run . 9223372036854775807 + 1
go run . -9223372036854775809 - 3
go run . 9223372036854775807 "*" 3
go run . 1 "*" 1
#1
go run . 1 "*" -1
#-1
