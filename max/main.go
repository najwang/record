package main

import "fmt"

func main() {
	a := []int{23, 123, 1, 11, 55, 93}
	max := Max(a)
	fmt.Println(max)
}

func Max(a []int) int {
	if len(a) == 0 {
		return 0
	}
	max := a[0]
	for _, c := range a {
		if c > max {
			max = c
		}
	}
	return max
}

// func Max(a []int) int {
// 	max := 0
// 	for i, g := range a {

// 		if i == 0 {
// 			max = g
// 			continue
// 		}

// 		if g >= max {
// 			max = g
// 		}
// 	}
// 	return max
// }
