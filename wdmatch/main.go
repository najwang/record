package main

import (
	"os"

	"github.com/01-edu/z01"
)

func main() {
	args := os.Args[1:]
	if len(args) != 2 {
		return
	}
	first := args[0]
	second := []rune(args[1])

	start := 0
	s := ""
	for _, e := range first {
		for i := start; i < len(second); i++ {
			letter := second[i]
			if letter == e {
				//z01.rintRune(letter)
				s += string(letter)
				start = i + 1
				break
			}
		}
	}
	if s == first {
		printString(s)
		z01.PrintRune('\n')
	}
}
func printString(s string) {
	for _, g := range s {
		z01.PrintRune(g)
	}
}
