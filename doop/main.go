package main

import (
	//"fmt"
	"os"
	//"github.com/01-edu/z01"
	// "math"
)

func main() {
	// fmt.Printf("MaxInt: %d\n", math.MaxInt)
	// 9223372036854775807
	// maxInt := 9223372036854775807
	args := os.Args[1:]
	if len(args) != 3 {
		return
	}

	first := args[0]
	second := args[1]
	third := args[2]

	num1, ok := Atoi(first)
	if !ok {
		return
	}
	num2, ok := Atoi(third)
	if !ok {
		return
	}

	text := "No division by 0"
	str := "No modulo by 0"

	result := 0
	switch second {
	case "+":
		result = num1 + num2
		if addOverflow(num1, num2) {
			return
		}
	case "-":
		result = num1 - num2
		if subtractionOverflow(num1, num2) {
			return
		}
	case "*":
		result = num1 * num2
		// handle multiplication overflow
		if result/num1 != num2 {
			return
		}
	case "/":
		if num2 == 0 {
			printString(text)
			// z01.PrintRune('\n')
			os.Stdout.WriteString("\n")
			return
		}
		result = num1 / num2
	case "%":
		if num2 == 0 {
			printString(str)
			// z01.PrintRune('\n')
			os.Stdout.WriteString("\n")
			return
		}
		result = num1 % num2
	default:
		return
	}

	Itoa(result)
	// fmt.Println(result)
	// z01.PrintRune('\n')
	os.Stdout.WriteString("\n")
}

func printString(s string) {
	for _, g := range s {
		// z01.PrintRune(g)
		os.Stdout.WriteString(string(g))
	}
}

func Atoi(s string) (int, bool) {
	rr := []rune(s)
	num := 0
	sign := 1
	for i, char := range rr {
		if i == 0 && char == '-' {
			sign = -1
			continue
		} else if i == 0 && char == '+' {
			sign = 1
			continue
		}
		if char < '0' || char > '9' {
			return 0, false
		}
		digit := int(char - '0')
		// num = num*10 + int(char-'0')
		num = num * 10
		if addOverflow(num, digit) {
			return 0, false
		}
		num += digit
	}
	return sign * num, true
}

func Itoa(nb int) {
	if nb == 0 {
		// z01.PrintRune('0')
		os.Stdout.WriteString("\n")
		return
	}
	isNegative := false
	if nb < 0 {
		isNegative = true
		nb = nb * -1
	}
	if isNegative {
		os.Stdout.WriteString(string('-'))
	}
	digit := make([]int, 0)
	for nb > 0 {
		digit = append(digit, nb%10)
		nb /= 10
	}
	for i := len(digit) - 1; i >= 0; i-- {
		os.Stdout.WriteString(string((rune(digit[i]) + '0')))
	}
}

func addOverflow(a, b int) bool {
	maxInt := 9223372036854775807
	return (a < 0 && b < maxInt-a) || (a > 0 && b > maxInt-a)
}

func subtractionOverflow(a, b int) bool {
	maxInt := 9223372036854775807
	return (b < 0 && a > maxInt+b) || (b > 0 && a < maxInt+b)
}
