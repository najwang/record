package main

import (
	"os"

	"github.com/01-edu/z01"
)

func main() {
	args := os.Args[1:]
	// b := len(args)
	// z01.PrintRune(rune(b + '0'))
	// z01.PrintRune('\n')
	// sum := 0
	// for _, b := range args {
	// 	v := []rune(b)
	// 	h := len(v)
	// 	sum += h
	// }
	//Num(sum)

	Num(len(args))
	z01.PrintRune('\n')
}
func Num(n int) {
	if n == 0 {
		z01.PrintRune('0')
		return
	}
	digit := make([]int, 0)
	for n > 0 {
		digit = append(digit, n%10)
		n /= 10
	}
	for i := len(digit) - 1; i >= 0; i-- {
		z01.PrintRune(rune(digit[i] + '0'))
	}
}
