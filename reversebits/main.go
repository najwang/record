package main

import (
	"fmt"
	// "os"
	// "strconv"
	// "github.com/01-edu/z01"
)

func ReverseBits(oct byte) byte {
	return reverseBits(int(oct))
}

func reverseBits(nb int) byte {
	str := ""
	if nb == 0 {
		str = "00000000"
	}

	for nb > 0 {
		digit := nb % 2
		nb /= 2
		str += string(rune(digit + '0'))
	}

	for len(str) < 8 {
		str += string('0')
	}

	ss := []rune(str)
	var out int = 0
	p := 0
	for i := len(ss) - 1; i >= 0; i-- {
		r := ss[i]
		digit := int(r - '0')
		out += digit * power2(p)
		p++
	}

	return byte(out)
}

func power2(n int) int {
	if n == 0 {
		return 1
	}
	return 2 * power2(n-1)
}

func main() {
	var input byte = 38
	var expected byte = 100

	if output := ReverseBits(input); output != expected {
		fmt.Printf("Test failed, expected: %d got %d\n", expected, output)
	} else {
		fmt.Printf("Test passed\n")
	}
}
