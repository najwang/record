package main

import (
	"fmt"
)

func main() {
	fmt.Println(Capitalize("Hello! How are you? How+are+things+4you?"))
}

func Capitalize(s string) string {
	sentence := []rune(s)
	for i, char := range sentence {
		if isAlpha(char) {
			if i == 0 || isAlpha(sentence[i-1]) == false {
				if sentence[i] >= 'a' && sentence[i] <= 'z' {
					sentence[i] = char - 32
				}
			} else {
				if sentence[i] >= 'A' && sentence[i] <= 'Z' {
					sentence[i] = char + 32
				}
			}
		}
	}
	return string(sentence)
}

func isAlpha(r rune) bool {
	if r >= 'a' && r <= 'z' || r >= '0' && r <= '9' || r >= 'A' && r <= 'Z' {
		return true
	}
	return false
}
